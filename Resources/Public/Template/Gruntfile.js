module.exports = function(grunt) {

    require( 'load-grunt-tasks' )( grunt );

    var path = require('path'),
        packagePath = path.resolve('../../../'),
        packagePathSplit = packagePath.split(path.sep),
        packageKey = packagePathSplit[packagePathSplit.length-1],
        compressableImageFormats = 'jpg,gif,svg,jpeg,png';

    // Icon/logo file paths
    var logoSourceFile = '../../Private/LogoSources/logo-default.ai',
        logoSourceFileAbsolute = path.resolve(logoSourceFile +'[0]'),
        iconSourceFile = '../../Private/LogoSources/icon-default.ai',
        iconSourceFileAbsolute = path.resolve(iconSourceFile +'[0]'),
        faviconTargetFileAbsolute = path.resolve(packagePath +'/Resources/Public/Template/images/favicon.ico'),
        packageIconTargetFileAbsolute = path.resolve(packagePath + '/ext_icon.gif'),
        backendLogoTargetFileAbsolute = path.resolve(packagePath +'/Resources/Public/Backend/Skin/img/logo_login.png');

    grunt.initConfig({

        pkg: grunt.file.readJSON('package.json'),

        // Adds vendor prefixes to given stylesheets.
        autoprefixer: {
            options: { },
            app: { src: 'css/app.css' },
            rte: { src: '../Backend/RTE.css'}
        },

        // Compiles Sass to CSS and generates necessary files if requested
        sass: {
            options: {
                sourceMap: true,
                sourceMapEmbed: true,
                sourceMapContents: true,
                includePaths: ['bower_components/foundation-sites/scss',
                               'bower_components/foundation-sites']
            },
            dist: {
                files: {
                    'css/app.css': 'sass/app.scss'
                }
            },
            // Generate RTE css
            rte: {
                files: {
                    '../Backend/RTE.css': 'sass/RTE.scss',
                }
            }
        },

        // Compress images job
        imagemin: {
            imageAssets: {
                files: [{
                    expand: true,
                    cwd: 'images/',
                    dest: 'images/',
                    src: ['**/*.{'+ compressableImageFormats +'}', '!**/*.min.*'],
                    rename: function(destinationPath, filename){
                        var dotPosition = filename.lastIndexOf('.');
                        var fileExtension = '';
                        if (dotPosition > -1) {
                            fileExtension = filename.substr(dotPosition+1);
                            filename = filename.substr(0, dotPosition);
                        }
                        return destinationPath + '/' + filename +'.min.'+ fileExtension;
                    }
                }]
            }
        },

        // Command line tasks
        // Mainly used for image / icon generation.
        exec: {
            generateFavicon: {
                command: 'convert -colorspace RGB -background transparent -define icon:auto-resize "' + iconSourceFileAbsolute + '" "'+ faviconTargetFileAbsolute +'"'
            },
            generatePackageIcon: {
                command: 'convert -colorspace RGB -alpha remove -antialias -background white "' + iconSourceFileAbsolute + '" "'+ packageIconTargetFileAbsolute +'"'
            },
            generateBackendLogo: {
                command: 'convert -colorspace RGB -background transparent -antialias -density 400 -resize 500 "' + logoSourceFileAbsolute + '" "'+ backendLogoTargetFileAbsolute +'"'
            }
        },


        // File change watcher
        watch: {
            grunt: {
                files: ['Gruntfile.js']
            },
            sass: {
                files: 'sass/**/*.scss',
                tasks: ['buildCSS'],
                options: {livereload:true}
            },
            images: {
                files: ['images/**/*.{'+ compressableImageFormats +'}', '!**/*.min.*'],
                tasks: ['compressImageAssets'],
                options: {
                    event: ['changed', 'added']
                }
            },
            iconSource: {
                files: [iconSourceFile],
                tasks: ['createIcons'],
                options: { event: ['changed'] }
            },
            logoSource: {
                files: [logoSourceFile],
                tasks: ['createBackendLogo'],
                options: { event: ['changed'] }
            },
        }

    });


    // Define tasks
    grunt.registerTask('compileSass', ['sass', 'autoprefixer']);
    grunt.registerTask('buildCSS', ['compileSass']);
    grunt.registerTask('compressImageAssets', ['imagemin:imageAssets']);
    grunt.registerTask('createIcons', ['exec:generateFavicon', 'exec:generatePackageIcon']);
    grunt.registerTask('createBackendLogo', ['exec:generateBackendLogo']);

    // Define default task
    grunt.registerTask('default', ['buildCSS', 'compressImageAssets', 'watch']);

}
