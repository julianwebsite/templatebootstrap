# Include original constants
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:powermail/Configuration/TypoScript/Main/constants.txt">

# Override/extend constants
plugin.tx_powermail{
    settings{
        javascript{
            addJQueryFromGoogle = 0
            powermailJQuery = ""
        }
    }
    view{
        templateRootPath = EXT:templatebootstrap/Resources/Private/Extensions/powermail/Templates/
        partialRootPath = EXT:templatebootstrap/Resources/Private/Extensions/powermail/Partials/
        layoutRootPath = EXT:templatebootstrap/Resources/Private/Extensions/powermail/Layouts/
    }
}