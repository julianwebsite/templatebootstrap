// Include original constants
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tt_news/pi/static/ts_new/constants.txt">

// Override/extend constants
plugin.tt_news.displayCatMenu.mode = tree
plugin.tt_news.displayCatMenu.showNewsCountForCategories = true
plugin.tt_news.displayCatMenu.expandable = false
$plugin.tt_news.displayCatMenu.expandAll = true
$plugin.tt_news.displayCatMenu.expandFirst = false