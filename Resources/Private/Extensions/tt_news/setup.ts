// Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:tt_news/pi/static/ts_new/setup.txt">
plugin.tt_news.code >
plugin.tt_news.code = LIST
// prevent indexing of the LIST view
config.index_enable = 0
[globalVar = GP:tx_ttnews|tt_news > 0]
// set code to SINGLE if the GETvar tx_ttnews[tt_news] exists
plugin.tt_news.code >
plugin.tt_news.code = SINGLE
// enable indexing of the SINGLE view
config.index_enable = 1


//CATEGORY FILTER CONFIGURATION
displayCatMenu {
// list of page IDs where the categories for the menu are stored (overrides GRSP if given)
        catPidList =
    // extend "catPidList" by the number of recursive levels
    recursive =

    // catmenu rendermode:
        // nestedWraps = make a catmenu with nested wraps
    // tree = use TYPO3 class treeview to build an expandable menu
    // ajaxtree = expandable menu with ajax (requires TYPO3 >= 4.1)

    mode = {$plugin.tt_news.displayCatMenu.mode}

// include prototype js library (required for catmenu mode "ajaxtree")
    includePrototypeJS = 0


    showNewsCountForCategories = {$plugin.tt_news.displayCatMenu.showNewsCountForCategories}

// enable expand/collapse for the category menu
    expandable = {$plugin.tt_news.displayCatMenu.expandable}
// completely expand the category menu on first load
    expandAll = {$plugin.tt_news.displayCatMenu.expandAll}
// keep the first level of the category menu expanded
    expandFirst = {$plugin.tt_news.displayCatMenu.expandFirst}

// stdWrap for the complete category menu
    catmenu_stdWrap.wrap = <div class="news-catmenu">|</div>

    // stdWrap for the category menu header in "nestedWraps" mode
    catmenuHeader_stdWrap.wrap = <p class="news-catmenu-header">|</p>
    
    // if set, the category menu header will not be displayed at all
    hideCatmenuHeader = 1
    
    // which icons to use for the category menu
    // 1 = icon from cat record
    // 2 = own icons
    // 0 = default icon
    // -1 = no icon
    catmenuIconMode = {$plugin.tt_news.displayCatMenu.catmenuIconMode}

// if "catmenuIconMode" = "2" all catmenu images (join, line ...) are expected in this path (relative)
    catmenuIconPath =

    // icon for all categories if "catmenuIconMode" = 2
    catmenuIconFile = EXT:tt_news/res/arrow.gif


    catmenuIconFile {
    // icon size for "catmenuIconMode" 1 & 2
        width = 18
        height = 16
    }

    // disables the "root" icon of the category tree
    catmenuNoRootIcon = {$plugin.tt_news.displayCatMenu.catmenuNoRootIcon}

// insert the category description as title attribute in catmenu links
    insertDescrAsTitle = 1

    // userdefined root icon
    catmenuRootIconFile = catmenuRootIconFile {
    // size of userdefined root icon
        width = 18
        height = 16
    }

    // wraps for active category links in the tree (only in mode "nestedWraps")
    catmenuItem_ACT_stdWrap.wrap = <div class="news-catmenu-ACT">|</div>

    // wraps for inactive category links in the tree (only in mode "nestedWraps")
    catmenuItem_NO_stdWrap.wrap = <div class="news-catmenu-NO">|</div>

    // wraps for each menu level (only in mode "nestedWraps")
    catmenuLevel1_stdWrap.wrap = <div class="level1">|</div>
    catmenuLevel2_stdWrap.wrap = <div class="level2">|</div>
    catmenuLevel3_stdWrap.wrap = <div class="level3">|</div>
    catmenuLevel4_stdWrap.wrap = <div class="level4">|</div>
    hideCatmenuHeader = 1
}