# Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/setup.txt">

# Override/extend setup
plugin.tx_news {
    settings {
        link {
            skipControllerAndAction = 1
        }
    }
}