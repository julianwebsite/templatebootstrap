# Include original constants
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:news/Configuration/TypoScript/constants.txt">

# Override/extend constants
plugin.tx_news{
    view {
        # cat=plugin.tx_news/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:templatebootstrap/Resources/Private/Extensions/news/Templates/
		# cat=plugin.tx_news/file; type=string; label=Path to template partials (FE)
         partialRootPath = EXT:templatebootstrap/Resources/Private/Extensions/news/Partials/
		# cat=plugin.tx_news/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:templatebootstrap/Resources/Private/Extensions/news/Layouts/
    }
}