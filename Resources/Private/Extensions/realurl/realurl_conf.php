<?php
$tx_realurl_config = [
    'init' => [
        'enableCHashCache' => true,
        'appendMissingSlash' => 'ifNotFile,redirect',
        'adminJumpToBackend' => true,
        'enableUrlDecodeCache' => false,
        'enableUrlEncodeCache' => false,
        'emptyUrlReturnValue' => '/',
    ],
    'pagePath' => [
        'type' => 'user',
        'userFunc' => 'EXT:realurl/class.tx_realurl_advanced.php:&tx_realurl_advanced->main',
        'spaceCharacter' => '-',
        'languageGetVar' => 'L',
        'rootpage_id' => '1',
    ],
    'fileName' => [
        'defaultToHTMLsuffixOnPrev' => 1,
        'acceptHTMLsuffix' => 1,
        'index' => [
            'print' => [
                'keyValues' => [
                    'type' => 98,
                ],
            ],
        ],
    ],
    'preVars' => [
        [
            /*'GETvar' => 'L',
            'valueMap' => [
                'de' => '0',
                'fr' => '1',
                'it' => '2',
                'en' => '3'
            ],
            'noMatch' => 'bypass',
            'valueDefault' => 'de',*/
        ],
    ],
    'postVarSets' => [
        '_DEFAULT' => [
            'news' => [
                0 => [
                    'GETvar' => 'tx_news_pi1[news]',
                    'lookUpTable' => [
                        'table' => 'tx_news_domain_model_news',
                        'id_field' => 'uid',
                        'alias_field' => 'title',
                        'useUniqueCache' => 1,
                        'useUniqueCache_conf' => [
                            'strtolower' => 1,
                            'spaceCharacter' => '-',
                        ],
                    ],
                ],
            ],
        ],
    ],
];

$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['realurl'] = [
    'www.julian.local' => $tx_realurl_config,
    'julian.local' => $tx_realurl_config,
    'www.julian.mu' => $tx_realurl_config,
    'julian.mu' => $tx_realurl_config
];

$TYPO3_CONF_VARS['EXTCONF']['realurl']['www.julian.local']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['julian.local']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['www.julian.mu']['pagePath']['rootpage_id'] = 1;
$TYPO3_CONF_VARS['EXTCONF']['realurl']['julian.mu']['pagePath']['rootpage_id'] = 1;

unset($tx_realurl_config);

//typo3conf/ext/templatebootstrap/Resources/Private/Extensions/realurl/realurl_conf.php