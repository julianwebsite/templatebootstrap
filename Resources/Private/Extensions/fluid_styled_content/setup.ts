# Include original setup
<INCLUDE_TYPOSCRIPT: source="FILE:EXT:fluid_styled_content/Configuration/TypoScript/Static/setup.txt">

# Override/extend setup
lib.fluidContent {
    templateRootPaths.10 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/fluid_styled_content/Templates/Content/
    layoutRootPaths.10 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/fluid_styled_content/Layouts/
    partialRootPaths.10 = EXT:{$plugin.templatebootstrap.packageKey}/Resources/Private/Extensions/fluid_styled_content/Partials/
}