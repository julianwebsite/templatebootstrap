# COPY/PASTE THIS INTO THE TYPO3 GRID ELEMENTS RECORD (after customizing)!
    backend_layout {
    colCount = 2
    rowCount = 1
    rows {
        1 {
            columns {
                1 {
                    name =
                        colPos = 0
                }
                2 {
                    name =
                        colPos = 1
                }
            }
        }
    }
}